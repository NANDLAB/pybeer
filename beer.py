#!/usr/bin/python3

import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
from os.path import expanduser
import time
import json
from playsound import playsound

activated = False

def getPress(buttons):
    for i, b in enumerate(buttons):
        if GPIO.input(b) == 0:
            return i
    return -1

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("beer/signal")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global activated
    print(msg.topic+" "+str(msg.payload))
    if (msg.topic == "beer/signal" and msg.payload.decode("utf-8") == "ACTIVATE"):
        activated = True

def main():
    global activated
    try:
        client = mqtt.Client("beer")

        brokerhost = "localhost"

        home = expanduser("~")

        soundBeerStart = home + "/riddle_beer_sounds/beer_start.mp3"
        soundBeerClick = home + "/riddle_beer_sounds/beer_click.mp3"
        soundBeerWrong = home + "/riddle_beer_sounds/beer_wrong.mp3"

        topic_beer = "beer/gameevent"
        payload_beer = "SOLVED"

        # Start the beer riddle when either startPin becomes LOW or an mqtt activate signal is received
        startPin = 6

        # Pin which indicates whether the riddle is solved.
        # Initialy High-Z, when solved becomes output LOW.
        solvedPin = 5

        # Input GPIOs with buttons attached
        buttons = [17, 27, 22]

        # Password
        pwd = [1, 2, 0, 2]

        print("Start pin:", startPin)
        print("Solved pin:", solvedPin)
        print("Dispenser button pins:", buttons)
        print("Password:", pwd)

        GPIO.setmode(GPIO.BCM)

        print("Setting up pins...")
        GPIO.setup(startPin, GPIO.IN, GPIO.PUD_UP)
        GPIO.setup(solvedPin, GPIO.IN, GPIO.PUD_UP)
        GPIO.setup(buttons, GPIO.IN, GPIO.PUD_UP)
        print("Done.")

        # Get MQTT configuration which includes broker hostname and port.
        config = {}
        configFileName = home + "/mqtt.json"
        try:
            with open(configFileName) as configFile:
                config = json.load(configFile)
            print("MQTT configuration from " + repr(configFileName) + ":")
            print(config)
            if "broker" in config:
                print("Config file has a broker attribute.")
                brokerhost = config['broker']
        except FileNotFoundError:
            print("Warning: File " + repr(configFileName) + " not found. Using default MQTT configuration.")

        print("Connecting to the broker: " + brokerhost)
        client.on_message = on_message
        client.on_connect = on_connect
        client.connect(brokerhost)
        client.loop_start();

        print("Waiting for start pin to become LOW or a mqtt activate signal...")
        while GPIO.input(startPin) == 1 and not activated:
            time.sleep(0.01)

        try:
            playsound(soundBeerStart, False)
        except:
            print("Cannot play beer start sound!")

        print("Beer riddle started.")

        print("Enter the password:")

        while True:
            correctInput = True
            for d in pwd:
                press = getPress(buttons)
                while press == -1:
                    time.sleep(0.01)
                    press = getPress(buttons)
                try:
                    playsound(soundBeerClick, False)
                except:
                    print("Cannot play beer click sound!")
                print(press)
                if press != d:
                    correctInput = False
                # Button release debouncing
                i = 0
                while i < 5:
                    if GPIO.input(buttons[press]) == 1:
                        i += 1
                    else:
                        i = 0
                    time.sleep(0.01)
            if correctInput:
                break
            else:
                try:
                    playsound(soundBeerWrong, False)
                except:
                    print("Cannot play beer wrong sound!")
                print("Try again!")

        print("Congratulations, riddle solved!")
        GPIO.setup(solvedPin, GPIO.OUT, initial=GPIO.LOW)
        print("Solved pin set to LOW.")
        print("Publishing string " + repr(payload_beer) + " on topic " + repr(topic_beer) + "...")
        try:
            client.publish(topic_beer, payload_beer)
            print("Done.")
        except:
            print("Failed to publish!")
        client.loop_stop();
        while True:
            pass
        # input("Press enter to exit...")
    except:
        raise
    finally:
        print("Cleaning up GPIO...")
        GPIO.cleanup()
        print("Done.")

main()

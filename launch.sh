#!/usr/bin/sh

riddles_kill() {
	cat "/home/pi/git/pybeer/riddles.sh" | while read -r riddle ; do
		echo "Stopping: $riddle"
		/usr/bin/pkill -f "/usr/bin/python3 /home/pi/git/pybeer/beer.py"
	done
}

riddles_start() {
	echo "Starting all the riddles..."
	cat "/home/pi/git/pybeer/riddles.sh" | while read -r riddle ; do
		echo "Starting: $riddle"
		/usr/bin/sh -c "$riddle" &
	done
}

main() {
	trap "riddles_kill ; exit" 1 2 3 9 15

	if [ ! -e "/home/pi/git/pybeer/riddles.sh" ] ; then
		echo "riddles.sh file does not exist!"
		exit 1
	fi

	broker=$(jq -r .broker /home/pi/mqtt.json)
	if [ -n "$broker" ] ; then
		echo "Broker hostname $broker was read from ~/mqtt.json ."
	else
		broker="localhost"
		echo "Cannot read ~/mqtt.json, using localhost as broker."
	fi

	echo "Launching all riddles..."
	riddles_kill
	sleep 1
	riddles_start

	echo "Listening for reset signal..."
	while true ; do
		/usr/bin/mosquitto_sub -L "mqtt://${broker}/beer/signal" | while read -r line ; do
			if [ "$line" = "RESET" ]; then
				echo "Reset signal received. Restarting all riddles..."
				riddles_kill
				sleep 1
				riddles_start
			fi
		done
		sleep 2
	done
}

main
